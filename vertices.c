#include <math.h>

#include <stdio.h>

#include "vertices.h"

static float
_vec3f_magnitude(const struct vec3f * vector) {
	return sqrtf((vector->x * vector->x) + (vector->y * vector->y) + (vector->z * vector->z));
}

static void
_vec3f_normal(const struct vec3f * vector, struct vec3f * out_result) {
	float vector_magnitude = _vec3f_magnitude(vector);
	(*out_result).x = vector->x / vector_magnitude;
	(*out_result).y = vector->y / vector_magnitude;
	(*out_result).z = vector->z / vector_magnitude;
}

static void
_vec3f_cross(const struct vec3f * a, const struct vec3f * b, struct vec3f * out_result) {
	float a_x = a->x, a_y = a->y, a_z = a->z;
	float b_x = b->x, b_y = b->y, b_z = b->z;
	 
	/* from Wikipedia (https://en.wikipedia.org/wiki/Cross_product#Coordinate_notation) */
	(*out_result).x = (a_y * b_z) - (a_z * b_y);
	(*out_result).y = (a_z * b_x) - (a_x * b_z);
	(*out_result).z = (a_x * b_y) - (a_y * b_x);
}

static float
_vec3f_dot(const struct vec3f * a, const struct vec3f * b) {
	/* from Wikipedia (https://en.wikipedia.org/wiki/Dot_product#Algebraic_definition) */
	return (a->x * b->x) + (a->y * b->y) + (a->z * b->z);
}

static void
_mat4f_identity(float * m) {
	MAT4_CM(m, 0, 0) = 1.0f; MAT4_CM(m, 1, 0) = 0.0f; MAT4_CM(m, 2, 0) = 0.0f; MAT4_CM(m, 3, 0) = 0.0f;
	MAT4_CM(m, 0, 1) = 0.0f; MAT4_CM(m, 1, 1) = 1.0f; MAT4_CM(m, 2, 1) = 0.0f; MAT4_CM(m, 3, 1) = 0.0f;
	MAT4_CM(m, 0, 2) = 0.0f; MAT4_CM(m, 1, 2) = 0.0f; MAT4_CM(m, 2, 2) = 1.0f; MAT4_CM(m, 3, 2) = 0.0f;
	MAT4_CM(m, 0, 3) = 0.0f; MAT4_CM(m, 1, 3) = 0.0f; MAT4_CM(m, 2, 3) = 0.0f; MAT4_CM(m, 3, 3) = 1.0f;
}

static void
_mat4f_multiply(float * a, float * b, float * o) {
	float
		a_1_1 = MAT4_CM(a, 0, 0), a_1_2 = MAT4_CM(a, 1, 0), a_1_3 = MAT4_CM(a, 2, 0), a_1_4 = MAT4_CM(a, 3, 0),
		a_2_1 = MAT4_CM(a, 0, 1), a_2_2 = MAT4_CM(a, 1, 1), a_2_3 = MAT4_CM(a, 2, 1), a_2_4 = MAT4_CM(a, 3, 1),
		a_3_1 = MAT4_CM(a, 0, 2), a_3_2 = MAT4_CM(a, 1, 2), a_3_3 = MAT4_CM(a, 2, 2), a_3_4 = MAT4_CM(a, 3, 2),
		a_4_1 = MAT4_CM(a, 0, 3), a_4_2 = MAT4_CM(a, 1, 3), a_4_3 = MAT4_CM(a, 2, 3), a_4_4 = MAT4_CM(a, 3, 3) 
	;

	float
		b_1_1 = MAT4_CM(b, 0, 0), b_1_2 = MAT4_CM(b, 1, 0), b_1_3 = MAT4_CM(b, 2, 0), b_1_4 = MAT4_CM(b, 3, 0),
		b_2_1 = MAT4_CM(b, 0, 1), b_2_2 = MAT4_CM(b, 1, 1), b_2_3 = MAT4_CM(b, 2, 1), b_2_4 = MAT4_CM(b, 3, 1),
		b_3_1 = MAT4_CM(b, 0, 2), b_3_2 = MAT4_CM(b, 1, 2), b_3_3 = MAT4_CM(b, 2, 2), b_3_4 = MAT4_CM(b, 3, 2),
		b_4_1 = MAT4_CM(b, 0, 3), b_4_2 = MAT4_CM(b, 1, 3), b_4_3 = MAT4_CM(b, 2, 3), b_4_4 = MAT4_CM(b, 3, 3)
	;

	/* from https://en.wikipedia.org/wiki/Matrix_multiplication#Definition */
	MAT4_CM(o, 0, 0) = (a_1_1*b_1_1) + (a_1_2*b_2_1) + (a_1_3*b_3_1) + (a_1_4*b_4_1);
	MAT4_CM(o, 1, 0) = (a_1_1*b_1_2) + (a_1_2*b_2_2) + (a_1_3*b_3_2) + (a_1_4*b_4_2);
	MAT4_CM(o, 2, 0) = (a_1_1*b_1_3) + (a_1_2*b_2_3) + (a_1_3*b_3_3) + (a_1_4*b_4_3);
	MAT4_CM(o, 3, 0) = (a_1_1*b_1_4) + (a_1_2*b_2_4) + (a_1_3*b_3_4) + (a_1_4*b_4_4);
	MAT4_CM(o, 0, 1) = (a_2_1*b_1_1) + (a_2_2*b_2_1) + (a_2_3*b_3_1) + (a_2_4*b_4_1);
	MAT4_CM(o, 1, 1) = (a_2_1*b_1_2) + (a_2_2*b_2_2) + (a_2_3*b_3_2) + (a_2_4*b_4_2);
	MAT4_CM(o, 2, 1) = (a_2_1*b_1_3) + (a_2_2*b_2_3) + (a_2_3*b_3_3) + (a_2_4*b_4_3);
	MAT4_CM(o, 3, 1) = (a_2_1*b_1_4) + (a_2_2*b_2_4) + (a_2_3*b_3_4) + (a_2_4*b_4_4);
	MAT4_CM(o, 0, 2) = (a_3_1*b_1_1) + (a_3_2*b_2_1) + (a_3_3*b_3_1) + (a_3_4*b_4_1);
	MAT4_CM(o, 1, 2) = (a_3_1*b_1_2) + (a_3_2*b_2_2) + (a_3_3*b_3_2) + (a_3_4*b_4_2);
	MAT4_CM(o, 2, 2) = (a_3_1*b_1_3) + (a_3_2*b_2_3) + (a_3_3*b_3_3) + (a_3_4*b_4_3);
	MAT4_CM(o, 3, 2) = (a_3_1*b_1_4) + (a_3_2*b_2_4) + (a_3_3*b_3_4) + (a_3_4*b_4_4);
	MAT4_CM(o, 0, 3) = (a_4_1*b_1_1) + (a_4_2*b_2_1) + (a_4_3*b_3_1) + (a_4_4*b_4_1);
	MAT4_CM(o, 1, 3) = (a_4_1*b_1_2) + (a_4_2*b_2_2) + (a_4_3*b_3_2) + (a_4_4*b_4_2);
	MAT4_CM(o, 2, 3) = (a_4_1*b_1_3) + (a_4_2*b_2_3) + (a_4_3*b_3_3) + (a_4_4*b_4_3);
	MAT4_CM(o, 3, 3) = (a_4_1*b_1_4) + (a_4_2*b_2_4) + (a_4_3*b_3_4) + (a_4_4*b_4_4);
}

static void
_mat4f_print(float * m) {
	for (size_t i=0; i<4; i++) {
		for (size_t j=0; j<4; j++) {
			printf("%G ", MAT4_CM(m, i, j));
		}
		printf("\n");
	}
}

static void
_view(const struct vec3f * eye, const struct vec3f * center, const struct vec3f * up, float * mvp_view) {
	/* equivelant to gluLookAt() followed by glTranslatef(). see: https://www.opengl.org/discussion_boards/showthread.php/130409-using-gluLookAt-properly */
	/* see OpenGL 2.1 gluLookAt(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml */
	/* see OpenGL 2.1 glTranslate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glTranslate.xml */

	struct vec3f f = {
		.x = center->x - eye->x,
		.y = center->y - eye->y,
		.z = center->z - eye->z,
	};
	struct vec3f f_norm;
	_vec3f_normal(&f, &f_norm);
	struct vec3f up_norm;
	_vec3f_normal(up, &up_norm);

	struct vec3f s;
	_vec3f_cross(&f_norm, &up_norm, &s);
	struct vec3f s_norm;
	_vec3f_normal(&s, &s_norm);

	struct vec3f u;
	_vec3f_cross(&s_norm, &f_norm, &u);

	struct vec3f eye_translate;
	eye_translate.x = _vec3f_dot(&s_norm, eye);
	eye_translate.x *= -1.0f;
	eye_translate.y = _vec3f_dot(&u, eye);
	eye_translate.y *= -1.0f;
	eye_translate.z = _vec3f_dot(&f_norm, eye);

	float * v = mvp_view;
	MAT4_CM(v, 0, 0) = s.x;          MAT4_CM(v, 1, 0) = s.y;          MAT4_CM(v, 2, 0) = s.z;          MAT4_CM(v, 3, 0) = eye_translate.x;
	MAT4_CM(v, 0, 1) = u.x;          MAT4_CM(v, 1, 1) = u.y;          MAT4_CM(v, 2, 1) = u.z;          MAT4_CM(v, 3, 1) = eye_translate.y;
	MAT4_CM(v, 0, 2) = -f_norm.x;    MAT4_CM(v, 1, 2) = -f_norm.y;    MAT4_CM(v, 2, 2) = -f_norm.z;    MAT4_CM(v, 3, 2) = eye_translate.z;
	MAT4_CM(v, 0, 3) = 0.0f;         MAT4_CM(v, 1, 3) = 0.0f;         MAT4_CM(v, 2, 3) = 0.0f;         MAT4_CM(v, 3, 3) = 1.0f;
}

static void
_projection(float fov_y, float z_near, float z_far, float * mvp_projection) {
	float aspect = 400.0f / 240.0f;  /* TODO remove magic constant */
	float f = 1.0f / tanf(fov_y / 2.0f);
	float * p = mvp_projection;
	MAT4_CM(p, 0, 0) = f / aspect;   MAT4_CM(p, 1, 0) = 0.0f;   MAT4_CM(p, 2, 0) = 0.0f;                                  MAT4_CM(p, 3, 0) = 0.0f;
	MAT4_CM(p, 0, 1) = 0.0f;         MAT4_CM(p, 1, 1) = f;      MAT4_CM(p, 2, 1) = 0.0f;                                  MAT4_CM(p, 3, 1) = 0.0f;
	MAT4_CM(p, 0, 2) = 0.0f;         MAT4_CM(p, 1, 2) = 0.0f;   MAT4_CM(p, 2, 2) = (z_far + z_near) / (z_near - z_far);   MAT4_CM(p, 3, 2) = (2 * z_far * z_near) / (z_near - z_far);
	MAT4_CM(p, 0, 3) = 0.0f;         MAT4_CM(p, 1, 3) = 0.0f;   MAT4_CM(p, 2, 3) = -1.0f;                                 MAT4_CM(p, 3, 3) = 0.0f;
}

static void
_mvp_calculate(struct mvp * out_mvp) {
	_mat4f_identity(out_mvp->mvp);
	_mat4f_multiply(out_mvp->mvp, out_mvp->projection, out_mvp->mvp);
	_mat4f_multiply(out_mvp->mvp, out_mvp->view,       out_mvp->mvp);
	_mat4f_multiply(out_mvp->mvp, out_mvp->model,      out_mvp->mvp);
}

void
mvp_init(
	/* projection args */ float fov_y, float z_near, float z_far,
	/* output          */ struct mvp * out_mvp
) {
	_mat4f_identity(out_mvp->model);
	_projection(fov_y, z_near, z_far, out_mvp->projection);
}

void
mvp_recalculate(
	struct vec3f camera_position, struct vec3f camera_target,
	struct mvp * out_mvp
) {
	struct vec3f up = {.x=0, .y=0, .z=1};
	_view(
		/* eye    */ &camera_position,
		/* center */ &camera_target,
		/* up     */ &up,
		out_mvp->view
	);

	_mvp_calculate(out_mvp);
}


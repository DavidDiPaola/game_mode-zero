#include <stdio.h>

#include <SDL2/SDL.h>

#define GL_GLEXT_PROTOTYPES 1
#include <SDL2/SDL_opengles2.h>

#include <stdint.h>

#include <endian.h>

#include "vertices.h"

#define LOG_ERROR_SDL() fprintf(stderr, "[ERROR] SDL (%s,%i): %s" "\n", __FILE__, __LINE__, SDL_GetError())

const size_t vec2_LENGTH = 2;

static struct {
	struct {
		SDL_Window    * window;
		SDL_GLContext   glcontext;
	} sdl;
	struct {
		struct mvp mvp;
		GLuint shader_program_id;
		GLint  shader_program_vu_mvp_id;
		GLint  shader_program_va_position_id;
		GLint  shader_program_va_texture_uv_id;
		GLint  shader_program_fu_texture_sampler_id;
	} gl;
} _state;

static int
_shader_compile(GLenum shader_type, const char * shader_SOURCECODE, GLuint * out_shader_id) {
	GLuint shader_id = glCreateShader(shader_type);
	glShaderSource(shader_id, 1, &shader_SOURCECODE, NULL);
	glCompileShader(shader_id);

	const char * const shader_type_STR = (shader_type == GL_VERTEX_SHADER) ? "vertex" : "fragment";

	GLsizei shader_log_length = 0;
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &shader_log_length);
	if (shader_log_length > 0) {
		GLchar * shader_log = calloc(shader_log_length, sizeof(GLchar));
		glGetShaderInfoLog(shader_id, shader_log_length, NULL, shader_log);
		fprintf(stderr, "[INFO] GL: shader log (%s shader, ID=%i): %s" "\n", shader_type_STR, shader_id, shader_log);
		free(shader_log);
	}

	GLint shader_compiled = GL_FALSE;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &shader_compiled);
	if (!shader_compiled) {
		fprintf(stderr, "[ERROR] GL: shader compiler (%s shader, ID=%i): compile failed" "\n", shader_type_STR, shader_id);
		return -1;
	}

	(*out_shader_id) = shader_id;
	return 0;
}

static int
_init(void) {
	int status;

	status = SDL_Init(SDL_INIT_VIDEO);
	if (status != 0) {
		LOG_ERROR_SDL();
		return -1;
	}

	_state.sdl.window = SDL_CreateWindow(
		"Mode Zero",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		400, 240,
		SDL_WINDOW_OPENGL
	);
	if (_state.sdl.window == NULL) {
		LOG_ERROR_SDL();
		return -2;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	_state.sdl.glcontext = SDL_GL_CreateContext(_state.sdl.window);
	if (_state.sdl.glcontext == NULL) {
		LOG_ERROR_SDL();
		return -3;
	}

	GLuint shader_vertex_id = 0;
	const GLchar * shader_vertex_SOURCECODE =
		"#version 100" "\n"
		"precision highp float;" "\n"
		"" "\n"
		"uniform   mat4 vu_mvp;" "\n"
		"attribute vec2 va_position;" "\n"
		"attribute vec2 va_texture_uv;" "\n"
		"" "\n"
		"varying vec2 fv_texture_uv;" "\n"
		"" "\n"
		"void main() {" "\n"
		"	gl_Position = vu_mvp * vec4(va_position, 0.0, 1.0);" "\n"
		"" "\n"
		"	fv_texture_uv = va_texture_uv;" "\n"
		"}" "\n"
	;
	_shader_compile(GL_VERTEX_SHADER, shader_vertex_SOURCECODE, &shader_vertex_id);

	GLuint shader_fragment_id = 0;
	const GLchar * shader_fragment_SOURCECODE =
		"#version 100" "\n"
		"precision lowp float;" "\n"
		"" "\n"
		"uniform sampler2D fu_texture_sampler;" "\n"
		"" "\n"
		"varying vec2 fv_texture_uv;" "\n"
		"" "\n"
		"void main() {" "\n"
		"	vec4 texel = texture2D(fu_texture_sampler, fv_texture_uv);" "\n"
		"	gl_FragColor = texel;" "\n"
		"}" "\n"
	;
	_shader_compile(GL_FRAGMENT_SHADER, shader_fragment_SOURCECODE, &shader_fragment_id);

	_state.gl.shader_program_id = glCreateProgram();
	glAttachShader(_state.gl.shader_program_id, shader_vertex_id);
	glAttachShader(_state.gl.shader_program_id, shader_fragment_id);
	glLinkProgram(_state.gl.shader_program_id);

	glDeleteShader(shader_vertex_id);
	glDeleteShader(shader_fragment_id);

	glUseProgram(_state.gl.shader_program_id);
	_state.gl.shader_program_vu_mvp_id             = glGetUniformLocation(_state.gl.shader_program_id, "vu_mvp");
	_state.gl.shader_program_va_position_id        = glGetAttribLocation( _state.gl.shader_program_id, "va_position");
	_state.gl.shader_program_va_texture_uv_id      = glGetAttribLocation( _state.gl.shader_program_id, "va_texture_uv");
	_state.gl.shader_program_fu_texture_sampler_id = glGetUniformLocation(_state.gl.shader_program_id, "fu_texture_sampler");

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	return 0;
}

static void
_close(void) {
	glDeleteProgram(_state.gl.shader_program_id);

	SDL_GL_DeleteContext(_state.sdl.glcontext);
	SDL_DestroyWindow(_state.sdl.window);
	SDL_Quit();
}

static void
_tile_load(const char * path, uint32_t * * out_tile) {
	FILE * file = fopen(path, "r+");

	size_t     tile_length = 32 * 32;
	uint32_t * tile = calloc(tile_length, sizeof(*tile));

	fread(tile, sizeof(*tile), tile_length, file);

	fclose(file);

	(*out_tile) = tile;
}

struct __attribute__((packed)) vec2f {
	GLfloat x;
	GLfloat y;
};

struct triangle {
	struct vec2f points[3];
};

struct quad {
	struct triangle triangles[2];
};

static int
_vertices_create(size_t map_width, size_t map_height, GLfloat * * out_vertices, size_t * out_vertices_length) {
	size_t quads_length = map_height * map_width;
	struct quad * quads = calloc(quads_length, sizeof(*quads));
	if (!quads) {
		perror("_quads_create(): calloc()");
		return -1;
	}

	GLfloat min = -1.0f;
	GLfloat max =  1.0f;
	GLfloat range = max - min;
	GLfloat delta_x = range / map_width;
	GLfloat delta_y = range / map_height;
	for (size_t y=0; y<map_height; y++) {
		for (size_t x=0; x<map_width; x++) {
			GLfloat left   = min + (x*delta_x);
			GLfloat right  = left + delta_x;
			GLfloat top    = max - (y*delta_y);
			GLfloat bottom = top - delta_y;;

			size_t i = (y*map_width) + x;
			quads[i].triangles[0] = (struct triangle){
				.points = {
					{ .x=left,  .y=top    },
					{ .x=left,  .y=bottom },
					{ .x=right, .y=top    },
				},
			};
			quads[i].triangles[1] = (struct triangle){
				.points = {
					{ .x=left,  .y=bottom },
					{ .x=right, .y=top    },
					{ .x=right, .y=bottom },
				},
			};
		}
	}

	(*out_vertices) = (GLfloat *)quads;
	(*out_vertices_length) = quads_length * (sizeof(struct quad)/sizeof(**out_vertices));
	return 0;
}

int
main() {
	int status;

	status = _init();
	if (status < 0) {
		return 1;
	}


	uint32_t * texturemap_data;
	_tile_load("./gfx/tilemap.bin", &texturemap_data);
	const size_t texturemap_width  = 32;
	const size_t texturemap_height = 32;
	GLuint texturemap_id = 0;
	glGenTextures(1, &texturemap_id);
	glBindTexture(GL_TEXTURE_2D, texturemap_id);
	glTexImage2D(
		GL_TEXTURE_2D,      /* target texture */
		0,                  /* level of detail (0=highest) */
		GL_RGBA,            /* internal format */
		texturemap_width,
		texturemap_height,
		0,                  /* border, must be 0 */
		GL_RGBA,            /* pixel data format */
		GL_UNSIGNED_BYTE,   /* pixel data type */
		texturemap_data
	);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	GLfloat * vertices;
	size_t    vertices_length;
	_vertices_create(2, 1, &vertices, &vertices_length);
	size_t    vertices_size = vertices_length * sizeof(*vertices);
	const GLenum vertices_type = GL_FLOAT;
	size_t vertices_count = vertices_length / vec2_LENGTH;
	GLuint vertices_buffer_id;
	glGenBuffers(1, &vertices_buffer_id);
	glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer_id);
	glBufferData(GL_ARRAY_BUFFER, vertices_size, vertices, GL_DYNAMIC_DRAW);  /* TODO static? */
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	const GLfloat uvs[] = {
		/* quad 0, triangle 0 */
		0.0f, 0.0f,  /* left,  top */
		0.0f, 0.5f,  /* left,  bottom */
		0.5f, 0.0f,  /* right, top */
		/* ..., triangle 1 */
		0.0f, 0.5f,  /* left,  bottom */
		0.5f, 0.0f,  /* right, top */
		0.5f, 0.5f,  /* right, bottom */

		/* quad 1, triangle 0 */
		0.5f, 0.0f,  /* left,  top */
		0.5f, 0.5f,  /* left,  bottom */
		1.0f, 0.0f,  /* right, top */
		/* ..., triangle 1 */
		0.5f, 0.5f,  /* left,  bottom */
		1.0f, 0.0f,  /* right, top */
		1.0f, 0.5f,  /* right, bottom */
	};
	const GLenum uvs_type = GL_FLOAT;
	const size_t uvs_count = (sizeof(uvs) / sizeof(*uvs)) / vec2_LENGTH;
	GLuint uvs_buffer_id;
	glGenBuffers(1, &uvs_buffer_id);
	glBindBuffer(GL_ARRAY_BUFFER, uvs_buffer_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if (vertices_count != uvs_count) {
		fprintf(stderr, "ERROR: vertices_count (%zi) != uvs_count (%zi)" "\n", vertices_count, uvs_count);
		_close();
	}

	struct vec3f camera_position = {.x= 0.0f, .y=-0.5f, .z= 0.5f};
	struct vec3f camera_target   = {.x= 0.0f, .y= 0.0f, .z= 0.0f};
	mvp_init(90, 0.1f, 1.0f, &_state.gl.mvp);

	int run = 1;
	while (run) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* load MVP matrix */
		mvp_recalculate(camera_position, camera_target, &_state.gl.mvp);
		glUniformMatrix4fv(_state.gl.shader_program_vu_mvp_id, 1, GL_FALSE, _state.gl.mvp.mvp);

		/* prep position buffer */
		glEnableVertexAttribArray(_state.gl.shader_program_va_position_id);
		glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer_id);
		glVertexAttribPointer(
			_state.gl.shader_program_va_position_id,
			vec2_LENGTH,          /* number of components per vertex attribute */
			vertices_type,        /* data type of each component in array */
			GL_FALSE,             /* whether should be normalized */
			0*sizeof(*vertices),  /* stride, byte offset between consecutive vertex attributes */
			(GLvoid *)0           /* offset to first component of first vertex attribute */
		);

		/* prep UV buffer */
		glEnableVertexAttribArray(_state.gl.shader_program_va_texture_uv_id);
		glBindBuffer(GL_ARRAY_BUFFER, uvs_buffer_id);
		glVertexAttribPointer(
			_state.gl.shader_program_va_texture_uv_id,
			vec2_LENGTH,     /* number of components per vertex attribute */
			uvs_type,        /* data type of each component in array */
			GL_FALSE,        /* whether should be normalized */
			0*sizeof(*uvs),  /* stride, byte offset between consecutive vertex attributes */
			(GLvoid *)0      /* offset to first component of first vertex attribute */
		);

		/* prep texture */
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texturemap_id);
		glUniform1i(_state.gl.shader_program_fu_texture_sampler_id, 0);

		/* draw */
		glDrawArrays(GL_TRIANGLES, 0, vertices_count);

		/* cleanup */
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(_state.gl.shader_program_va_position_id);
		glDisableVertexAttribArray(_state.gl.shader_program_va_texture_uv_id);

		SDL_GL_SwapWindow(_state.sdl.window);

		SDL_Event event;
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) {
			run = 0;
		}
		else if (event.type == SDL_KEYDOWN) {
			switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				run = 0;
				break;

				case SDLK_UP:
				camera_position.y += 0.01f;
				camera_target.y += 0.01f;
				break;

				case SDLK_DOWN:
				camera_position.y -= 0.01f;
				camera_target.y -= 0.01f;
				break;

				case SDLK_LEFT:
				camera_position.x -= 0.01f;
				camera_target.x -= 0.01f;
				break;

				case SDLK_RIGHT:
				camera_position.x += 0.01f;
				camera_target.x += 0.01f;
				break;

				case SDLK_PAGEUP:
				camera_position.z += 0.01f;
				//camera_target.z += 0.01f;
				break;

				case SDLK_PAGEDOWN:
				camera_position.z -= 0.01f;
				//camera_target.z -= 0.01f;
				break;
			}
		}
	}

	glDeleteBuffers(1, &vertices_buffer_id);
	glDeleteBuffers(1, &uvs_buffer_id);

	_close();
	return 0;
}


SRC = main.c vertices.c
GFX = gfx/tilemap.bin
BIN ?= mode-zero

OBJ = $(SRC:.c=.o)
LIB = sdl2 gl
_CFLAGS = \
	-Wall -Wextra \
	-O2 \
	-fwrapv \
	$(shell pkg-config --cflags $(LIB)) \
	$(CFLAGS)
_LDFLAGS = $(LDFLAGS)
_LDFLAGS_LIB = $(shell pkg-config --libs $(LIB)) -lm

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)
	rm -f $(GFX)
	$(MAKE) -C tool/ clean

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ) $(GFX)
	$(CC) $(_LDFLAGS) $(OBJ) -o $@ $(_LDFLAGS_LIB)

tool/ff2bin32:
	$(MAKE) -C tool/ ff2bin32

%.bin: %.png tool/ff2bin32
	cat $< | png2ff | ./tool/ff2bin32 - $@


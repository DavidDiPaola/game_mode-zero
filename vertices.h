#ifndef _VERTICES_H
#define _VERTICES_H

/* macro to store matrix values in column-major format (used by OpenGL) */
#define MAT4_CM(array, col, row) (array)[((col)*4)+(row)]

struct vec3f {
	float x;
	float y;
	float z;
};

struct mvp {
	float model[4*4];
	float view[4*4];
	float projection[4*4];
	float mvp[4*4];
};

void
mvp_init(
	/* projection args */ float fov_y, float z_near, float z_far,
	/* output          */ struct mvp * out_mvp
);

void
mvp_recalculate(
	struct vec3f camera_position, struct vec3f camera_target,
	struct mvp * out_mvp
);

#endif


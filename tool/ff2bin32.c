#include <stdio.h>

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>

#include <stdint.h>

#include "ff.h"

#define _ERROR "ERROR: ff2bin32"

int
main(int argc, char * * argv) {
	int status;

	if (argc < 3) {
		fprintf(stderr, "syntax: %s <input file|-> <output file|->" "\n", argv[0]);
		return 1;
	}
	const char * ARG_INPUT  = argv[1];
	const char * ARG_OUTPUT = argv[2];

	int fd_input = STDIN_FILENO;
	if (strcmp(ARG_INPUT, "-") != 0) {
		fd_input = open(ARG_INPUT, O_RDONLY);
		if (fd_input < 0) {
			perror(_ERROR);
			return 1;
		}
	}

	int fd_output = STDOUT_FILENO;
	if (strcmp(ARG_OUTPUT, "-") != 0) {
		fd_output = open(ARG_OUTPUT, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
		if (fd_output < 0) {
			perror(_ERROR);
			return 2;
		}
	}

	size_t width  = 0;
	size_t height = 0;
	status = ff_read_header(fd_input, &width, &height);
	if (status < 0) {
		fprintf(stderr, _ERROR ": failed to read header" "\n");
		return 3;
	}

	uint32_t * pixels = NULL;
	status = ff_read_pixels_u32(fd_input, width, height, &pixels);
	if (status < 0) {
		fprintf(stderr, _ERROR ": failed to read pixels" "\n");
		return 4;
	}

	for (size_t i=0; i<(width*height); i++) {
		uint32_t pixel_be = htobe32(pixels[i]);
		ssize_t amount = write(fd_output, &pixel_be, sizeof(pixel_be));
		if (amount < 0) {
			perror(_ERROR ": writing pixels");
			return 5;
		}
	}

	return 0;
}

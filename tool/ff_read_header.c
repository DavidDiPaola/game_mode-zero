#include <sys/types.h>
#include <unistd.h>

#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>

#include <stdio.h>

#include <string.h>

#include "ff.h"

#define _ERROR "ERROR: ff_read_header()"

int
ff_read_header(
	int fd,
	size_t * out_width, size_t * out_height
) {
	ssize_t amount;

	off_t offset = lseek(fd, FF_OFFSET_HEADER, SEEK_SET);
	if (offset < 0) {
		if (errno != ESPIPE) {  /* if we can't seek, then we have to trust that the current offset is OK */
			perror(_ERROR);
			return -1;
		}
	}
	else if (offset != FF_OFFSET_HEADER) {
		fprintf(stderr, _ERROR ": seek didn't work" "\n");
		return -2;
	}

	struct ff_header header;
	amount = read(fd, &header, sizeof(header));
	if (amount < 0) {
		perror(_ERROR);
		return -3;
	}
	else if ((size_t)amount < sizeof(header)) {
		fprintf(stderr, _ERROR ": couldn't read entire header" "\n");
		return -4;
	}
	if (strcmp(header.magic, "farbfeld") != 0) {
		fprintf(stderr, _ERROR ": incorrect header magic number" "\n");
		return -5;
	}
	header.width  = be32toh(header.width);
	header.height = be32toh(header.height);

	(*out_width)  = header.width;
	(*out_height) = header.height;
	return 0;
}


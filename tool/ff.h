#ifndef _FF_H
#define _FF_H

#include <stdint.h>

struct ff_header {
	char magic[8];
	uint32_t width;
	uint32_t height;
};

#define FF_WORDSPERPIXEL 4
#define FF_OFFSET_HEADER 0
#define FF_OFFSET_PIXELS sizeof(struct ff_header)

int
ff_read_header(
	int fd,
	size_t * out_width, size_t * out_height
);

int
ff_read_pixels_u32(
	int fd, size_t width, size_t height,
	uint32_t * * out_pixels
);

#endif


#include <stdint.h>

#include <stdio.h>

#include <stdlib.h>

#include <unistd.h>

#include <errno.h>

#include "ff.h"

#define _ERROR "ERROR: ff_read_pixels_u32()"

int
ff_read_pixels_u32(
	int fd, size_t width, size_t height,
	uint32_t * * out_pixels
) {
	int status;

	uint16_t * ff_pixels;
	size_t     ff_pixels_length = FF_WORDSPERPIXEL * width * height;
	size_t     ff_pixels_size   = ff_pixels_length * sizeof(*ff_pixels);
	ff_pixels = calloc(ff_pixels_length, sizeof(*ff_pixels));
	if (ff_pixels == NULL) {
		perror(_ERROR ": calloc");
		return -1;
	}

	off_t offset = lseek(fd, FF_OFFSET_PIXELS, SEEK_SET);
	if (offset < 0) {
		if (errno != ESPIPE) {  /* if we can't seek, then we have to trust that the current offset is OK */
			perror(_ERROR);
			return -2;
		}
	}
	else if (offset != FF_OFFSET_PIXELS) {
		fprintf(stderr, _ERROR ": seek didn't work" "\n");
		return -3;
	}

	ssize_t amount = read(fd, ff_pixels, ff_pixels_size);
	if (amount < 0) {
		perror(_ERROR ": pixel read");
		return -4;
	}
	else if ((size_t)amount < ff_pixels_size) {
		fprintf(stderr, _ERROR ": pixel read: couldn't read all pixels" "\n");
		return -5;
	}

	size_t     pixels_length = ff_pixels_length / FF_WORDSPERPIXEL;
	uint32_t * pixels = calloc(pixels_length, sizeof(*pixels));
	for (size_t i=0; i<ff_pixels_length; i+=FF_WORDSPERPIXEL) {
		uint8_t red   = (be16toh(ff_pixels[i+0]) >> 8);
		uint8_t green = (be16toh(ff_pixels[i+1]) >> 8);
		uint8_t blue  = (be16toh(ff_pixels[i+2]) >> 8);
		uint8_t alpha = 0xFF;

		pixels[i/FF_WORDSPERPIXEL] = (red << 24) | (green << 16) | (blue << 8) | (alpha << 0);
	}

	free(ff_pixels);

	(*out_pixels) = pixels;
	return 0;
}

